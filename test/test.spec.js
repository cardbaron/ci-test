const chai = require('chai')
const expect = chai.expect
const request = require('request')
const app = require('../src/server.js')
const port = 3000

let server

//async req
const arequest = async (value) => new Promise((resolve,reject) =>{
    request(value, (error, response) => {
        if(error) reject(error)
        else resolve(response)
    })
})

describe('Test Rest API', ()=>{
    beforeEach("Start server", async () => {
        server = app.listen(port)
    })
    describe("Test functionality", () => {
        it("get /add?a=5&b=3 returns 8", async () => {
            const options = {
                method: 'GET',
                url: 'http://localhost:3000/add',
                qs: {a: '5', b: '3'},
            };
            await arequest(options).then((res)=>{
                console.log({message: res.body})
                expect(res.body).to.equal('8')
            }).catch(()=>{
                console.log(res)
                expect(true).to.equal(false, "add function failed")
            })
        })
    })

    afterEach(()=>{
        server.close()
    })
})